/*A script for getting data from the aws api that gets its data from the dynamodb for lamps.*/
$(document).ready(function(){
	var apigClient = apigClientFactory.newClient();
	
	function assign_table(id,status,temp,brightness){
		$("#working_lamp").html("<td>"+id+"</td><td>"+status+"</td><td>"+temp+"</td><td>"+brightness+"</td>");
	}

	function get_value(val){
		var params = {
			'id':val
		};
		
		var body = {
		
		};
		
		var additionalParams = {
			headers:{
			
			},
			queryParams:{
				'id':val
			}
		};
		apigClient.lampresGet(params,body,additionalParams).then(function(result){
			/*Output data in log*/
			console.log(result.data.Item);
			
			/*Assign values to variables*/
			var id = result.data.Item.LampID;
			var status = result.data.Item.Status;
			var temp = result.data.Item.Temperature;
			var brightness = result.data.Item.Brightness;
			
			/*Set them to the table*/
			assign_table(id,status,temp,brightness);
		}).catch(function(result){
			console.log("error");
		});
	}
	
	/*By default without any change in select value, call getvalue for lamp1*/
	get_value(1);
	/*When change in select value*/
	$("#lamp_id").change(function(){
		var val = $("#lamp_id").val();
		
		/*Call the get value method*/
		get_value(val);
	});
	
	/*Change table status when reset button is clicked*/
	$(document).on("click","#reset",function(){
		get_value(1);
	});
});