/*Now we had to change the entire layout. The new layout consists of the input fields inside of the table rows instead of divs*/

$(document).ready(function(){
	
	/*This function toggles between the schedule display. This function either displays rows or sets theri display to NONE*/
	function toggle(value){
		
		/*So if the value is 1, then the editable schedules pop up .i.e. their display property is set to null. CAUTION: DO NOT SET THEM TO BLOCK, setting them to block will contract them and make them appear in an unwanted manner (They will not appear as spearate columns, but will appear as a single column). So just set them to null. That will work perfectly.*/
		if(value == 1){
			$(".editable_s1").css("display","");
			$(".editable_s2").css("display","");
			$(".editable_s3").css("display","");
			$(".non-editable_s1").css("display","none");
			$(".non-editable_s2").css("display","none");
			$(".non-editable_s3").css("display","none");
		}else if(value == 0){
			
			/*Just do the opposite if the value is 0*/
			$(".editable_s1").css("display","none");
			$(".editable_s2").css("display","none");
			$(".editable_s3").css("display","none");
			$(".non-editable_s1").css("display","");
			$(".non-editable_s2").css("display","");
			$(".non-editable_s3").css("display","");
		}
	}
	
	/*This event will fire up everytime the dimming panel(checkbox) is changed.*/
	$("#dimming_panel").change(function(){
		
		/*If checked then the value in the if statement will be one and toggle function will have an arguement of one. Otherwise toggle function will have a value of 0*/
		if(this.checked){
			toggle(1);
		}else{
			toggle(0);
		}
	});
});