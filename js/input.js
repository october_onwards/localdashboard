/*Please start form the submit onclick event handler and work your way up the functions accordingly. It's at the bottom. Just look for START HERE*/

$(document).ready(function(){
	//global json data
	var data = {'val_lamp_id':1,'val_Hr_ton':0,'val_Min_ton':0,'val_to_intensity':192,'val_s1_hr':0,'val_s1_min':0,'val_s1_intensity':192,'val_s2_hr':0,'val_s2_min':0,'val_s2_intensity':192,'val_s2_hr':0,'val_s3_hr':0,'val_s3_min':0,'val_s3_intensity':192,'val_Hr_toff':0,'val_Min_toff':0};
	
	//change global json data upon change in any select value. This sounds dumb but there's a technique
	
	
	/*So instead of creating independant event handlers for each select input. We use a single function which differentiates between select inputs and updates the global json data accordingly. This saves alot of code. Also anything worth repeating is worth automating.*/
	$('select').change(function(){
		
		/*As we know there are multiple select values and this event handler will trigger if any one of them changes. So to differentiate between select inputs, we have assigned unique ids to each select values.*/
		
		/*This line will get the id values of the select inputs. So this way we will know exactly which select value is changed*/
		var id = $(this).attr('id');
		
		/*Now that we know the id values we will just update the global json data by taking the value of the selected id*/
		data['val_'+id] = $("#"+id).val();
	});
	
	function ajax_post(){
		/*finally its time to assign our global json data to the another json data that we will stringify and post to aws. The reason for creating another json object is just so that our real global json data is safe*/
		var para = {
			                 "state":{
				"desired":{
					"Lamp": data['val_lamp_id'],               
					"Time1H": data['val_Hr_ton'],         
					"Time1M": data['val_Min_ton'],      
					"Time2H": data['val_s1_hr'],         
					"Time2M": data['val_s1_min'],        
					"Time3H": data['val_s2_hr'],         
					"Time3M": data['val_s1_min'],        
					"Time4H": data['val_s3_hr'],         
					"Time4M": data['val_s3_min'],        
					"Time5H": data['val_Hr_toff'],         
					"Time5M": data['val_Min_toff'],       
					"Time1B": data['val_to_intensity'],  
					"Time2B": data['val_s1_intensity'],
					"Time3B": data['val_s2_intensity'],
					"Time4B": data['val_s3_intensity'],
					"Test1": 5,
					"Test2": 5
				}
			}
		};
		
		
		/*So basically this functioni was already built, So I don't need to explain it :) */
		
		/*Problems: jsonp doesnot work with asynchronus post. So we loose that. And application/json is considered non standard and is replaced by application/x-www-form-urlencoded*/
		//var myJSON = JSON.stringify(obj);
		
        $.ajax({
		   type: "POST",
           url:  "http://127.0.0.1:1880/" ,
           data:para,
           crossDomain : true,
           //dataType: 'string',
		   //beforeSend: function(xhr){xhr.setRequestHeader("Authorization","key='qYDrCjsvXW7UM4hnNybgY9OGJlWNgkPo5rIYD7ma'");},
           headers :{
                   //'Content-Type':'text/plain;charset=UTF-8',
					//Authorization:'key=qYDrCjsvXW7UM4hnNybgY9OGJlWNgkPo5rIYD7ma'
					//'x-api-key':'qYDrCjsvXW7UM4hnNybgY9OGJlWNgkPo5rIYD7ma'
					//'cache-control':'no-cache'
                    },
           success: function(result) {
				/*Hide spinner*/	
				$("#spinner").css("display","none");
                alert("Lamp Set");
           }
		});
		  
	}
	
	/*This function is only there to output global json data to the console so that we know everythings working. Just for debugging. Nothing much :) */
	function display(){
		var value;
		for(var key in data){
			value = data[key];
			console.log(key,value);
		}
	}
	
	/*The function is solely dedicated to assigning only hour values via for loop, Just for modularity and debugging purposes. I Will integrate hour, minute and intensity function into a single one pretty soon.*/
	function hour(val){
		/*If val is one, the values from s[i]_hour,s[i]_hour and s[i]_hour will be assigned. Where i=1,2 and 3.*/
		if(val){
			for(var i=1; i<=3;i++){
				data['val_s'+i+'_hr'] = $("#s"+i+"_hr").val();
			}
		}else{
			for(var i=1; i<=3;i++){
				data['val_s'+i+'_hr'] = $("#Hr_ton").val();
			}
		}
	}
	
	/*The function is solely dedicated to assigning only minute values via for loop, Just for modularity and debugging purposes. I Will integrate hour, minute and intensity function into a single one pretty soon.*/
	function minute(val){
		
		/*If val is one, the values from s[i]_min,s[i]_min and s[i]_min will be assigned. Where i=1,2 and 3.*/
		if(val){
			for(var i=1; i<=3;i++){
				data['val_s'+i+'_min'] = $("#s"+i+"_min").val();
			}
		}else{
			/*Values of Min_ton is assigned to all s[i]_min*/
			for(var i=1; i<=3;i++){
				data['val_s'+i+'_min'] = $("#Min_ton").val();
			}
		}
	}
	
	/*The function is solely dedicated to assigning only intensity values via for loop, Just for modularity and debugging purposes. I Will integrate hour, minute and intensity function into a single one pretty soon.*/
	function intensity(val){
		/*If val is one, the values from s[i]_intensity,s[i]_intensity and s[i]_intensity will be assigned. Where i=1,2 and 3.*/
		if(val){
			for(var i=1; i<=3;i++){
				data['val_s'+i+'_intensity'] = $("#s"+i+"_intensity").val();
			}
		}else{
			for(var i=1; i<=3;i++){
				data['val_s'+i+'_intensity'] = $("#to_intensity").val();
			}
		}
	}
	
	/*This function has an arguement which is either set to one or zero. If one, then it will assign hour values, mimnute values and intensity values as per selected values of the option fields. It will then display the result in the console and finally call ajax_post. Else it will assign the values of Time_ON to schedule 1,2 and 3.*/
	function setvalue(val){
		if(val){
			hour(1);
			minute(1);
			intensity(1);
			display();
			ajax_post();
		}else{
			hour(0);
			minute(0);
			intensity(0);
			display();
			ajax_post();
		}
	}
	
	/*A function to check the checkbox status.*/
	function check_val(){
		
		/*jQuery always constructs an array of matches instead of selecting only raw dom elements like javascript. Try it with multiple input fields with the same type. Javascript will always select the first match whereas jQuery will create an array out of all the matches. In our code there is only one input field with the name dim_panel which is the checkbox itself, but even though there is only a single checkbox, jQuery will create an array with a single element. So here jQuery attempts to create an array of all input[type=name] that are "CHECKED". If the checkbox is checked the array will have one element in it. If the checkbox is unchecked the array will not have  any elements in it and hence its length will be zero. This method is always preferred over using document.getElementsByName["dim_panel"].checked.*/
		var check_stat = $("input[name=dim_panel]:checked").length; 
		
		/*if the length of the array is greater than zero it will return true which makes the event handler call setvalue(1) else the event handler will call setvalue(0) */
		if(check_stat>0){
			return true;
		}else{
			return false;
		}
	}
	
	
	/*START HERE*/
	/*this will first check checkbox status by calling check_val function; if clicked then it will change the global json data to the select values, else it will assign s1,s2 and s3 the value of Hr_ton,Min_ton and to_intensity. Then finally it will post the json data.*/
	$(document).on("click","#submit",function(){
		/*Display spinner*/
		$("#spinner").css("display","");
		if(check_val()){
			//this just means the set value will now assign individual input values
			setvalue(1);
		}else{
			//this assigns the default time on values to the schedule inputs
			setvalue(0);
		}
	});
	
	
	
	/*reset button to reset form values since the form is not refreshing after submit*/
	
	/*NOTE: upon reset all the input values will get reset. So theoretically upon submitting again the default values should get submitted. However theres a small bug. All the input values are reset, but upon submitting the last entered value gets submitted only for lamp id time on and time off. All other values are reset and function perfectly*/
	$(document).on("click","#reset",function(){
		//alert("reset will be made");

		$('select').each(function(){
			
			/*This line will just select the 0th option value which in our case are the default values*/
			$(this).prop('selectedIndex',0);
			
			/*So dont get confused with the following line. The below line is only to reset the lamp id, time on and time off values explicitly. And only these three values are to be reset manually because all other inputs are functioning correctly. These three are the only two fields that still output the last entered values upon submit even after getting reset.*/
			
			/*This is for reset the lamp id*/
			data['val_lamp_id'] = 1;

			/*These three are the time on values*/
			data['val_Hr_ton'] = 0;
			data['val_Min_ton'] = 0;
			data['val_to_intensity'] = 192;
			
			/*These two are the time off values*/
			data['val_Hr_toff'] = 0;
			data['val_Min_toff'] = 0;
			
		});
	});
});

/*NOTE: While the reset method is called you need to call get_value method from get.js with an arguement of 1. Do it at the last so that all other activities will occur before. Then finally after everything is done, remove all the console output values.*/